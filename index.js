/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function displayWelcomeMessage() {
		let fullName = prompt("What is your name? ");
		let userAge = prompt("How young are you?");
		let userLocation = prompt("Where do you live?");
		
		alert("Thank you for your input!")

		console.log("Hello, " + fullName + "!");
		console.log("You are " + userAge +" years old.");
		console.log("You live in " + userLocation + ".");
	}

	displayWelcomeMessage();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function diplayFavArtists() {
		let firstArtist = "IU";
		let secondArtist = "Aj Rafael";
		let thirdArtist = "Gabe Bondoc";
		let fourthArtist = "Ben&Ben";
		let fifthArtist = "December Avenue";

		console.log("1. " + firstArtist);
		console.log("2. " + secondArtist);
		console.log("3. " + thirdArtist);
		console.log("4. " + fourthArtist);
		console.log("5. " + fifthArtist);
	}

	diplayFavArtists();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function printFavMovies() {
		let firstMovie = "Grave of the Fireflies (1988)";
		let firstMovieRating = 95;
		let secondMovie = "Seven (1995)";
		let secondMovieRating = 95;
		let thirdMovie = "I Used To Be Famous (2022)";
		let thirdMovieRating = 87;
		let fourthMovie = "The Social Network (2010)";
		let fourthMovieRating = 87;
		let fifthMovie = "Hachiko: A Dog's Story (2009)";
		let fifthMovieRating = 84;

		console.log("1. " + firstMovie);
		console.log("Rotten Tomatoes Audience Score: " + firstMovieRating + "%");
		console.log("2. " + secondMovie);
		console.log("Rotten Tomatoes Audience Score: " + secondMovieRating + "%");
		console.log("3. " + thirdMovie);
		console.log("Rotten Tomatoes Audience Score: " + thirdMovieRating + "%");
		console.log("4. " + fourthMovie);
		console.log("Rotten Tomatoes Audience Score: " + fourthMovieRating + "%");
		console.log("5. " + fifthMovie);
		console.log("Rotten Tomatoes Audience Score: " + fifthMovieRating + "%");
	}

	printFavMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

	let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
	}

printFriends();
